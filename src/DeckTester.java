public class DeckTester {
    public static void main (String[] args) {
        String[] ranks = { "A", "B", "C" };
        String[] suits = { "Giraffes", "Lions" };
        int[] values = { 2, 1, 6 };
        Deck deck = new Deck(ranks, suits, values);

        deck.shuffle();
        while (!deck.isEmpty()) {
            System.out.println(deck.deal());
        }
    }
}
