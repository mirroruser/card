import java.util.ArrayList;
import java.util.List;

public class Deck {
    private List<Card> unDealt;
    private List<Card> dealt;

    public Deck(String[] ranks, String[] suits, int[] values) {
        List<Card> deck = new ArrayList();

        for (int s = 0; s < suits.length; s++) {
            for (int i = 0; i < ranks.length; i++) {
                deck.add(new Card(ranks[i], suits[s], values[i]));
            }
        }
        this.unDealt = deck;
        this.dealt = new ArrayList();
    }

    public boolean isEmpty() {
        return this.unDealt.size() <= 0;
    }

    public int size() {
        return this.unDealt.size();
    }

    public Card deal() {
        if (this.unDealt.size() <= 0) return null;
        Card currentCard = this.unDealt.remove(0);
        this.dealt.add(currentCard);
        return currentCard;
    }

    public void shuffle() {
        this.unDealt.addAll(this.dealt);
        this.dealt.clear();
        for (int i = 0; i < this.unDealt.size(); i++) {
            Card tmp = this.unDealt.get(i);
            int rand = (int) (Math.random() * this.unDealt.size());
            this.unDealt.set(i, this.unDealt.get(rand));
            this.unDealt.set(rand, tmp);
        }
    }
}
