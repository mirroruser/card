public class Card {
    private String rank;
    private String suit;
    private int pointValue;

    public Card(String rank, String suit, int pointValue) {
        this.rank = rank;
        this.suit = suit;
        this.pointValue = pointValue;
    }

    public String getRank() {
        return this.rank;
    }

    public String getSuit() {
        return this.suit;
    }

    public int getPointValue() {
        return this.pointValue;
    }

    public boolean equals(Card otherCard) {
        return this.rank.equals(otherCard.rank) && this.suit.equals(otherCard.suit);
    }

    public String toString() {
        return "Suit: " + this.suit + ", Rank: " + this.rank;
    }
}
